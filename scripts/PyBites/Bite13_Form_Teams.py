# Bite 17. Form teams from a group of friends
# https://codechalleng.es/bites/17/

'''Write a function called friends_teams that takes a list of friends,
a team_size (type int, default=2) and order_does_matter (type bool, default False).

Return all possible teams. Hint: if order matters (order_does_matter=True),
the number of teams would be greater.

See the tests for more details. Enjoy :)'''

import itertools
team = "dave larry steve ed bob mike".split()
friends = 'Bob Dante Julian Martin'.split()

def friends_teams(friends, team_size=2, order_does_matter=True):
    permutate = itertools.permutations(friends, team_size)
    combinate = itertools.combinations(friends, team_size)

    if order_does_matter is True:
        print("Permutations")
        return list(permutate)
    else:
        print("Combination")
        return list(combinate)

for team in friends_teams(friends, 2, True):
    print(team)


# PyBites Solution:
def friends_teams_2(friends, team_size=2, order_does_matter=True):
    if order_does_matter:
        func = itertools.permutations
    else:
        func = itertools.combinations
    return func(friends, team_size)


# another interesting solution
def friends_teams_2(friends, team_size=2, order_does_matter=True):
    operation = itertools.permutations if order_does_matter else itertools.combinations
    return operation(friends, team_size)



