# PyBite64 Fix a truncating zip function
# https://codechalleng.es/bites/64/

"""('Tim', 'DE', False)
('Bob', 'ES', True)
('Julian', 'AUS', True)
('Carmen', 'NL', False)
('Sofia', 'BR', True)
What?! Mike, Kim, and Andre are missing!
You heard somebody mention itertools the other day for its power to work with iterators.
Maybe it has a convenient way to solve this problem?
Check out the module and patch the get_attendees function for Bert so it returns all names again.
For missing data use dashes (-). After the fix Bert should see this output:

('Tim', 'DE', False)
('Bob', 'ES', True)
('Julian', 'AUS', True)
('Carmen', 'NL', False)
('Sofia', 'BR', True)
('Mike', 'US', '-')
('Kim', '-', '-')
('Andre', '-', '-')
Good luck, Bert will be grateful if you fix this bug for him!
By the way, this won't be the last itertools Bite, it is a power tool you want to become familiar with!"""


import itertools

names = 'Tim Bob Julian Carmen Sofia Mike Kim Andre'.split()
locations = 'DE ES AUS NL BR US'.split()
confirmed = [False, True, True, False, True]

def get_attendees():
    for participants in itertools.zip_longest(names, locations, confirmed, fillvalue="-"):
        print(participants)

# Example looping over multiple iterables
def traverse_list_in_parallel():
    for n, l, c, in itertools.zip_longest(names, locations, confirmed, fillvalue='-'):
        print("\nNAME: ", n)
        print("LOCATION: ", l)
        print(conzz)

if __name__ == '__main__':
    get_attendees()