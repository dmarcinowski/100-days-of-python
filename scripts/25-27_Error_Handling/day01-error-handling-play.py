import sys

# Error Handling Play
# from https://realpython.com/python-exceptions/

def linux_interaction():
    assert('linux' in sys.platform), "Function can only run on Linux systems"
    print("Do something")


try:
    linux_interaction()
except:
    #pass # program does not crash, but has no output
    # Below print statement tells us the function did not run, but is not specific to the exception
    print("Linux Function was not executed®")


# Another example for try/except block
try:
    linux_interaction()
except AssertionError as error:
    print(error)
    print("the linux interaction() function was not executed")

print()
try:
    with open('file.log') as file:
        read_data = file.read()
except FileNotFoundError as fnf_error:
    print(fnf_error)

print("\n\n")

def do_stuff_with_number(n):
    print(n)

def catch_this():
    the_list = "1 2 3 4 5".split()
    for i in range(40):
        try:
            do_stuff_with_number(the_list[i])
        except IndexError as e:
            do_stuff_with_number(0)
            #print(e)


do_stuff_with_number(10)
catch_this()


actor = {"name": "John Cleese", "rank": "awesome"}

print(type(actor))

print(actor["name"].split()[1])

