# Bite 107. Filter numbers with a list comprehension
# https://codechalleng.es/bites/107/

# Complete the function below that receives a list of numbers and returns only the even numbers that are > 0 and even (divisible by 2).
# The challenge here is to use Python's elegant list comprehension feature to return this with one line of code (while writing readable code).

# traditional for loop
# numbers = list(range(-10,20))
#
# for x in numbers:
#     if (x % 2 == 0) and (x > 0):
#         print(x)

numbers = list(range(-10, 11))
def filter_positive_even_numbers(numbers):
    """Receives a list of numbers, and returns a filtered list of only the
       numbers that are both positive and even (divisible by 2), try to use a
       list comprehension."""

    return[x for x in numbers if (x % 2 == 0) and (x > 0)]
# PyBites solution
    # return [n for n in numbers if n > 0 and n % 2 == 0]
print(filter_positive_even_numbers(numbers))

